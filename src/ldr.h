#ifndef INCLUDE_RELOADER_H
#define INCLUDE_RELOADER_H

/**
 * The name of the API object that must be in reloadable libraries.
 */
#define LDR_API_SYM   "LDR_API"


/**
 * A macro for the declaration of the API object. It should be used to define
 * the API in the library's header, and to initialize it in the body.
 */
#define LDR_API_DECL  const struct ldrapi_t LDR_API


/**
 * A set of functions to facilitate library reloading. Every reloadable library
 * must have a global variable whose name matches <LDR_API_SYM>. This is used
 * to invoke that library's implementation-specific reload logic.
 */
struct ldrapi_t {
    /* Return a new 'state' for the library. This must be non-NULL. It is used
     * when loading the library for the first time. When use of the library is
     * finished, this will be passed to <finalize> to clean up. */
    void *(*init)(void);

    /* NOTE:
     * The <state> parameter in the following functions is passed the value
     * returned by the <init> routine. */

    /* Clean up and destroy a 'state' for the library. */
    void (*finalize)(void *state);

    /* Called every time the library is reloaded. */
    void (*reload)(void *state);

    /* Called every time the library is about to be reloaded, if it has already
     * been loaded for the first time. */
    void (*unload)(void *state);
};


/**
 * An object used to reload a specific library on demand. Each reloadable
 * library will be managed by a single <struct ldr_t>.
 */
struct ldr_t;


enum ldrerr_t {
    LDR_OK = 0,
    LDR_NO_API,      /* No API symbol was found in the library. */
    LDR_BAD_PATH,    /* The path could not be <stat>'d. */
    LDR_LOAD_FAILED, /* The <dlopen> call failed. */
    LDR_NOT_LOADED,  /* <ldr_getsym> was called on an non-loaded library. */
    LDR_NO_SYM,      /* The symbol requested by <ldr_getsym> wasn't found. */
};


/**
 * Allocate a new loader object that operates on the library at the given path.
 * The loader keeps a separate, interal copy of <lib_path>. If there is any
 * error, then NULL is returned. Any non-NULL pointer returned must be freed by
 * <ldr_destroy> at the end of its life.
 */
struct ldr_t *ldr_create(const char *lib_path);


/**
 * Clean up and free any memory associated with a loader object created via
 * <ldr_create>. If the associated library is still loaded, it is unloaded
 * during this process. The pointer at <loader_ptr> is then set to NULL.
 */
void ldr_destroy(struct ldr_t **loader_ptr);


/**
 * Load the library associated with this loader object. The first time this is
 * invoked, the initialization function <ldrapi_t.init> of the library will be
 * called. On successive calls, the library is merely reloaded via
 * <ldrapi_t.unload> and <ldrapi_t.load>, using the already-created state.
 */
void ldr_load(struct ldr_t *loader);


/**
 * Unload (and not reload) the library associated with this loader object. The
 * library can be loaded again via <ldr_load>, but will be treated as if it is
 * being loaded for the very first time.
 */
void ldr_unload(struct ldr_t *loader);


/**
 * Return a pointer to the object named <symbol> in the library associated with
 * this loader object. If the library is not loaded, or no such symbol is found,
 * then NULL is returned.
 */
void *ldr_getsym(struct ldr_t *loader, const char *symbol);


/**
 * Return the library path managed by this loader object.
 */
const char *ldr_getpath(struct ldr_t *loader);


/**
 * Return the pointer to the library's current state (see <struct ldrapi_t>).
 * If the library is not loaded, then this will return NULL.
 */
void *ldr_getstate(struct ldr_t *loader);


/**
 * Retrieve the last error encountered while operating on a loader object. For
 * its string representation, see <ldr_strerror>.
 */
enum ldrerr_t ldr_errno(const struct ldr_t *loader);


/**
 * Return the human-readable string of an error code. For a loader object, this
 * can be retrieved via <ldr_errno>.
 */
const char *ldr_strerror(enum ldrerr_t err);

#endif
