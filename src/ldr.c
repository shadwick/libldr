#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <dlfcn.h>

#include "ldr.h"


struct ldr_t {
    const char *path;
    void *handle;
    ino_t inode;
    struct ldrapi_t api;
    void *state;
    enum ldrerr_t last_err;
};


struct ldr_t *ldr_create(const char *lib_path)
{
    struct ldr_t *loader = calloc(1, sizeof(struct ldr_t));
    if (loader != NULL) {
        loader->path = malloc(strlen(lib_path) + 1);
        if (loader->path != NULL)
            strcpy((char *) loader->path, lib_path);
        else {
            free(loader);
            loader = NULL;
        }
    }
    return loader;
}


void ldr_destroy(struct ldr_t **loader_ptr)
{
    assert(loader_ptr != NULL);

    struct ldr_t *loader = *loader_ptr;
    if (loader != NULL) {
        ldr_unload(loader);

        if (loader->path != NULL)
            free((char *) loader->path);

        free(loader);
        *loader_ptr = NULL;
    }
}


void ldr_load(struct ldr_t *loader)
{
    assert(loader != NULL);
    assert(loader->path != NULL);

    struct stat attr;

    if (stat(loader->path, &attr) != 0) {
        /* If we couldn't stat the path, then it doesn't exist. */
        loader->last_err = LDR_BAD_PATH;
    }
    else if (loader->inode == attr.st_ino) {
        /* The inode is the same as from the last load - no changes needed. */
        loader->last_err = LDR_OK;
    }
    else {
        /* If the library is already open, unload and close it first. */
        if (loader->handle != NULL) {
            loader->api.unload(loader->state);
            dlclose(loader->handle);
        }

        void *new_handle = dlopen(loader->path, RTLD_NOW);
        if (new_handle != NULL) {
            loader->handle = new_handle;
            loader->inode = attr.st_ino;

            /* Copy over the API function table from the library. */
            const struct ldrapi_t *new_api = dlsym(loader->handle, LDR_API_SYM);
            if (new_api != NULL) {
                loader->api = *new_api;

                /* If there is no state yet, then this is the first time and we
                 * invoke the initialization routine. */
                if (loader->state == NULL)
                    loader->state = loader->api.init();

                loader->api.reload(loader->state);

                loader->last_err = LDR_OK;
            }
            else {
                /* No API function table was found so we cannot proceed. */
                dlclose(loader->handle);
                loader->handle = NULL;
                loader->inode = 0;
                loader->last_err = LDR_NO_API;
            }
        }
        else {
            /* The library could not be loaded. Either the path is non-existent
             * or the file format was unrecognized. */
            loader->handle = NULL;
            loader->inode = 0;
            loader->last_err = LDR_LOAD_FAILED;
        }
    }
}


void ldr_unload(struct ldr_t *loader)
{
    assert(loader != NULL);
    assert(loader->path != NULL);

    if (loader->handle != NULL) {
        loader->api.finalize(loader->state);
        loader->state = NULL;

        dlclose(loader->handle);
        loader->handle = NULL;

        loader->inode = 0;
    }

    loader->last_err = LDR_OK;
}


void *ldr_getsym(struct ldr_t *loader, const char *sym_name)
{
    assert(loader != NULL);

    void *result;

    if (loader->handle != NULL) {
        result = dlsym(loader->handle, (char *) sym_name);
        if (result != NULL)
            loader->last_err = LDR_OK;
        else
            loader->last_err = LDR_NO_SYM;
    }
    else {
        /* The library is not currently loaded; no symbols can be retrieved. */
        result = NULL;
        loader->last_err = LDR_NOT_LOADED;
    }

    return result;
}


const char *ldr_getpath(struct ldr_t *loader)
{
    assert(loader != NULL);

    return loader->path;
}


void *ldr_getstate(struct ldr_t *loader)
{
    assert(loader != NULL);

    return loader->state;
}


enum ldrerr_t ldr_errno(const struct ldr_t *loader)
{
    assert(loader != NULL);

    return loader->last_err;
}


const char *ldr_strerror(enum ldrerr_t err)
{
    switch (err) {
    case LDR_OK:
        return "OK";
    case LDR_NO_API:
        return "No API symbol ('" LDR_API_SYM "') found";
    case LDR_BAD_PATH:
        return "Invalid or non-existent path";
    case LDR_LOAD_FAILED:
        return "Failed to load path";
    case LDR_NOT_LOADED:
        return "Library is not currently loaded";
    case LDR_NO_SYM:
        return "Requested symbol not found";
    default:
        return "Unknown error code";
    }
}
