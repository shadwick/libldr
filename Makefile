GCC=gcc
LD=gcc
AR=ar

CFLAGS=-Wall -Wextra -std=c99 -Wno-unused -O3 -fPIC
LFLAGS=-shared

SRC_DIR=src
OBJ_DIR=obj
LIB_DIR=lib

OBJ_FILES=$(patsubst %,$(OBJ_DIR)/%,ldr.o)

SO_LIB=libldr.so
A_LIB=libldr.a


all: $(LIB_DIR)/$(SO_LIB) $(LIB_DIR)/$(A_LIB)

$(LIB_DIR)/$(SO_LIB): $(OBJ_DIR) $(OBJ_FILES) $(LIB_DIR)
	$(LD) -o $@ $(LFLAGS) $(OBJ_FILES)
	strip -x $@

$(LIB_DIR)/$(A_LIB): $(OBJ_DIR) $(OBJ_FILES) $(LIB_DIR)
	$(AR) rcs $@ $(OBJ_FILES)
	strip -x $@

$(OBJ_DIR):
	mkdir -p $@

$(LIB_DIR):
	mkdir -p $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) -c -o $@ $(CFLAGS) $<

clean:
	rm -rf $(OBJ_DIR) $(LIB_DIR)
